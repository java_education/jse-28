package ru.t1.oskinea.tm.exception.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException(@NotNull final String message) {
        super(message);
    }

    @SuppressWarnings("unused")
    public AbstractUserException(@NotNull final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    @SuppressWarnings("unused")
    public AbstractUserException(@Nullable final Throwable cause) {
        super(cause);
    }

    @SuppressWarnings("unused")
    public AbstractUserException(
            @NotNull final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
